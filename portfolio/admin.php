<?php
include("connection.php");
session_start();
if(isset($_SESSION['adminid'])){
        header("Location:admin-projects.php");
    }
?>
<!DOCTYPE HTML>

<html>

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sign In</title>
    <link rel="shortcut icon" href="assets/image/favicon.ico">
    <link rel="stylesheet" href="assets/css/signup.css">
    <link rel="stylesheet" href="assets/bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
</head>

<body>
<div class="row" style="margin-left:0;margin-right:0;">
    <div class="col col-md-4 offset-md-4 col-sm-12 signup-form" style="margin-top: 8%;">
        <form id="signin-form" action="admin-login-db.php" method="post">
            <h2>Admin Sign In</h2>
            <hr>
            <?php if(isset($_GET["signinerror"])){ echo '<pre class="signuperror">Invalid Username or Password!</pre>'; } ?>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" required="required">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" required="required">
            </div>
            <span style="color:red;margin-left:25%;margin-top:1%;margin-bottom:1%;"></span>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-lg" name="login">Sign In</button>
            </div>
        </form>
    </div> 
</div>
    
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>
    $( "#signin-form" ).validate({});
    </script>
    <script src="assets/js/myjs.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery-3.3.1.min.js"></script>

</body>

</html>