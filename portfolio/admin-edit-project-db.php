<?php
session_start();
include("connection.php");

if(isset($_REQUEST["edit"]))
{
    $id = $_REQUEST['id'];
    $title = $_REQUEST["title"];
    $description = $_REQUEST["description"];
    
    if($_FILES['image']['name']!=""){
        
        $random1 = substr(number_format(time() * rand(),0,'',''),0,45); 

        $results = explode('.',$_FILES['image']['name']);
        $extension = end($results);

        $pic1= $random1.".".$extension; // I do not know if there is an ext option in the $files array though!

        $target = "assets/images/" . $pic1; 

        move_uploaded_file($_FILES['image']['tmp_name'], $target);  


        $stmt = $conn->prepare("UPDATE project SET title=?,description=?,image=? WHERE id=?");
        $stmt->bind_param("sssi", $title,$description,$pic1,$id);

        if($stmt->execute()){
            header("Location:admin-projects.php");
        }else{
        	

        }
    }else{
        $stmt = $conn->prepare("UPDATE project SET title=?,description=? WHERE id=?");
        $stmt->bind_param("ssi", $title,$description,$id);

        if($stmt->execute()){
            header("Location:admin-projects.php");
        }else{

        }
    }

    
}


?>