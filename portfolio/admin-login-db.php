<?php
session_start();
include("connection.php");

if(isset($_REQUEST["login"]))
{
    $email = $_REQUEST["email"];
    $password = $_REQUEST["password"];
    
    $stmt = $conn->prepare("select id,name from admin where email = ? and password = ?");
    $stmt->bind_param("ss", $email,$password);
    
    $stmt->execute();
    
    $stmt->bind_result($id,$fullname);

    if($stmt->fetch())
    {
        $_SESSION["adminid"] = $id;
        $_SESSION["adminname"] = $fullname;
        
        header("location:admin-projects.php");
    }
    else{
        header("location:admin.php?signinerror=1");
    }
}


?>