<?php
    session_start();
    include("connection.php");
    if(!isset($_SESSION['adminid'])){
        header("Location:admin.php");
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Add Project | Admin Panel</title>

    <!-- Bootstrap core CSS -->
      <link href="assets/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
        .iconss{
            padding:5px;
            cursor: pointer;
        }
        .iconss:hover{
            color: #02A;
        }
        .form-group{
            margin-bottom: 0;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="assets/css/dashboard.css" rel="stylesheet">
  </head>
  <body>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#" style="color:#efefef !important;">Portfolio Admin</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="admin-projects.php">Projects <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="admin-add-new-project.php">Add new Project</a>
              </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="admin-logout.php">Logout</a>
                </li>
            </ul>
          </div>
        </nav>
      
      
    <div class="container-fluid">
      <div class="row">

        <main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1 class="h2">Add Project</h1>
          </div>
          <div class="container formcontainer" style="    width: 50%;">
            <form method="post" action="admin-add-project-db.php" enctype="multipart/form-data">
                <label for="title">Title:</label>
                <br>
                <input type="text" class="form-control" id="title" name="title" required>
                <br>
                <label for="desc">Description:</label>
                <textarea style="width:100%;height:200px;resize:none;" name="description" id="desc"  class="form-control" required></textarea>
                <label for="image">Image:</label>
                <br>
                <input type="file" id="image" name="image"  class="form-control" required>
                <br>
                <div style="text-align:center;">
                    <input type="submit" class="btn btn-primary" value="Add Project" name="add">
                </div>
            </form>  
          </div>

        </main>
      </div>
    </div>
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
