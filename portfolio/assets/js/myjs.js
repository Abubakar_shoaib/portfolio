function admin_del_project(projid){
    var result = confirm("Are you sure you want to delete?");
    if(result==true){
        $.ajax({
            url:"deleteproject.php",
            data:{"projid":projid},
            method:"POST",
            success:function(response){
                if(response=="true"){
                    alert("Project Deleted Successfully!");
                    window.location.reload();
                    
                }
            },
            failure:function(){

            }
        });
    }else{
        
    }
}

function admin_edit_project(id){
    location.href = "admin-edit-project.php?id="+id;
}