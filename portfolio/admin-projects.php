<?php
    session_start();
    include("connection.php");
    if(!isset($_SESSION['adminid'])){
        header("Location:admin.php");
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Projects | Admin Panel</title>

    <!-- Bootstrap core CSS -->
      <link href="assets/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
        .iconss{
            padding:5px;
            cursor: pointer;
        }
        .iconss:hover{
            color: #02A;
        }
        .form-group{
            margin-bottom: 0;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="assets/css/dashboard.css" rel="stylesheet">
  </head>
  <body>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#" style="color:#efefef !important;">Portfolio Admin</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link active" href="admin-projects.php">Projects <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="admin-add-new-project.php">Add new Project</a>
              </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="admin-logout.php">Logout</a>
                </li>
            </ul>
          </div>
        </nav>
      
      
    <div class="container-fluid">
      <div class="row">

        <main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1 class="h2">Projects</h1>
          </div>
            
          <div class="table-responsive" style="margin-top:30px;">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody>
                <?php
                    $stmt = $conn->prepare("SELECT id,title,description,image FROM project ORDER BY id DESC");
                    $stmt->execute();
                    $stmt->store_result();
                    if(($stmt->num_rows)>0){
                        $stmt->bind_result($id,$title,$desc,$image);
                        while($stmt->fetch()){
                  
                          
                ?>
                     <tr>
                  
                       <td><?php echo $id;?></td>
                       <td><?php echo $title;?></td>
                       <td><?php echo $desc;?></td>
                         <td><a target="_blank" href="assets/images/<?php echo $image;?>"><img src="assets/images/<?php echo $image;?>" style="height:100px;width:100px;"></a></td>
                       <td>
                          <i class="far fa-edit iconss" onclick="admin_edit_project(<?php echo $id;?>);"></i>
                          <i class="fas fa-trash iconss" onclick="admin_del_project(<?php echo $id;?>);"></i>
                       </td>
                
                <?php 
                        }
                    }  
                ?>
              </tbody>
            </table>
          </div>

        </main>
      </div>
    </div>
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/myjs.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
