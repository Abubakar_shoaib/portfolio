<?php
    include("connection.php");
?>
<!doctype html>
<html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <title>Portfolio</title>
  </head>
  <body>
 
 <nav class="navbar navbar-expand-lg navbar-light bg-light ">

 
  <a class="navbar-brand" href="index.php"><h2 style="padding-left: 70px; margin-top: 7px">Portfolio</h2></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.php" style="padding-left: 750px;"><b>Home</b> <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="#projtitle" "><b>Projects</b></a>
      <a class="nav-item nav-link" href="#container"><b>Portfolio</b></a>
      
    </div>
  </div>
  
</nav>
</br>

      <div class="container" id="container">
          <div class="jumbotron">
              <h1 class="display-4">Hello, world!</h1>
              <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
              <hr class="my-4">
              <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
              <p class="lead">
            
              </p>
          </div>
          <section>
              <div class="projtitle" id="projtitle"><h1>Projects</h1></div>
              <div class="row projectrow">
                  <?php
                    $stmt = $conn->prepare("SELECT id,title,description,image FROM project ORDER BY id DESC");
                    $stmt->execute();
                    $stmt->store_result();
                    if(($stmt->num_rows)>0){
                        $stmt->bind_result($id,$title,$desc,$image);
                        while($stmt->fetch()){
                  
                          
                ?>
                  <div class="col-md-4 col-sm-12 projcol">
                    <div class="card" style="height:500px;">
                      <img class="card-img-top" style="width:100%;height:270px;" src="assets/images/<?php echo $image;?>" alt="Card image cap">
                      <div class="card-body">
                        <h5 class="card-title"><?php echo $title;?></h5>
                        <p class="card-text" style="    overflow-y: auto;
    height: 150px;"><?php echo $desc;?></p>
                        
                      </div>
                    </div>
                  </div>
                  
                  <?php 
                        }
                    }
                  ?>
                  
              </div>
            
          </section>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="assets/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  </body>
</html>