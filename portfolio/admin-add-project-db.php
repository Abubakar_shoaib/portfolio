<?php
session_start();
include("connection.php");

if(isset($_REQUEST["add"]))
{
    $title = $_REQUEST["title"];
    $description = $_REQUEST["description"];
    
    $random1 = substr(number_format(time() * rand(),0,'',''),0,45); 
    
    $results = explode('.',$_FILES['image']['name']);
    $extension = end($results);
    
    $pic1= $random1.".".$extension; // I do not know if there is an ext option in the $files array though!

    $target = "assets/images/" . $pic1; 

    move_uploaded_file($_FILES['image']['tmp_name'], $target);  
    
    
    $stmt = $conn->prepare("INSERT INTO project(title,image,description) VALUES(?,?,?)");
    $stmt->bind_param("sss", $title,$pic1,$description);
    
    if($stmt->execute()){
        header("Location:admin-projects.php");
    }else{
        
    }
    
    
}


?>